#
# For a description of the syntax of this configuration file,
# see the file kconfig-language.txt in the NuttX tools repository.
#

config CANUTILS_UAVCAN
	bool "UAVCAN Library"
	default n
	depends on STM32_HAVE_CAN1
	depends on !STM32_CAN1
	depends on !STM32_CAN2
	depends on (STM32_HAVE_TIM2 && !STM32_TIM2) || (STM32_HAVE_TIM3 && !STM32_TIM3) || (STM32_HAVE_TIM4 && !STM32_TIM4) || (STM32_HAVE_TIM5 && !STM32_TIM5) || (STM32_HAVE_TIM6 && !STM32_TIM6) || (STM32_HAVE_TIM7 && !STM32_TIM7)
	depends on C99_BOOL8
	depends on HAVE_CXX
	depends on !DISABLE_POLL
	---help---
		Enables support for the UAVCAN library.

if CANUTILS_UAVCAN

config UAVCAN_LIBUAVCAN_URL
	string "UAVCAN URL"
	default "https://github.com/UAVCAN/libuavcan/archive"
	---help---
		UAVCAN URL.

config UAVCAN_LIBUAVCAN_VERSION
	string "UAVCAN Version"
	default "531433a3261ff1568e824c240d0f1c6ecef73be1"
	---help---
		UAVCAN version.

config UAVCAN_DSDL_URL
	string "DSDL URL"
	default "https://github.com/UAVCAN/dsdl/archive"
	---help---
		DSDL URL.

config UAVCAN_DSDL_VERSION
	string "DSDL Version"
	default "9804a3e6972825586be252ce08dd899f44994b14"
	---help---
		DSDL version.

config UAVCAN_PYUAVCAN_URL
	string "Python UAVCAN URL"
	default "https://github.com/UAVCAN/pyuavcan/archive"
	---help---
		Python UAVCAN URL.

config UAVCAN_PYUAVCAN_VERSION
	string "Python UAVCAN Version"
	default "4e2798ec3da8e8493b769da514f3b96eea5773e2"
	---help---
		Python UAVCAN version.

config UAVCAN_STM32_NUM_IFACES
	int "Number of CAN Interfaces"
	default 1
	range 1 1 if !STM32_HAVE_CAN2
	range 1 2 if STM32_HAVE_CAN2

if UAVCAN_STM32_TIMER_NUMBER = 2 && STM32_TIM2
comment "Timer 2 is already configured for NuttX"
endif
if UAVCAN_STM32_TIMER_NUMBER = 3 && STM32_TIM3
comment "Timer 3 is already configured for NuttX"
endif
if UAVCAN_STM32_TIMER_NUMBER = 4 && STM32_TIM4
comment "Timer 4 is already configured for NuttX"
endif
if UAVCAN_STM32_TIMER_NUMBER = 5 && STM32_TIM5
comment "Timer 5 is already configured for NuttX"
endif
if UAVCAN_STM32_TIMER_NUMBER = 6 && STM32_TIM6
comment "Timer 6 is already configured for NuttX"
endif
if UAVCAN_STM32_TIMER_NUMBER = 7 && STM32_TIM7
comment "Timer 7 is already configured for NuttX"
endif

config UAVCAN_STM32_TIMER_NUMBER
	int "Timer Number"
	default 2 if STM32_HAVE_TIM2 && !STM32_TIM2
	default 3 if STM32_HAVE_TIM3 && !STM32_TIM3
	default 4 if STM32_HAVE_TIM4 && !STM32_TIM4
	default 5 if STM32_HAVE_TIM5 && !STM32_TIM5
	default 6 if STM32_HAVE_TIM6 && !STM32_TIM6
	default 7 if STM32_HAVE_TIM7 && !STM32_TIM7
	range 2 7
	---help---
		Specifies the timer number.

choice
	prompt "C++ Version"
	default UAVCAN_CPP03

config UAVCAN_CPP03
	bool "C++03"
	---help---
		The library will use C++03.

config UAVCAN_CPP11
	bool "C++11"
	---help---
		The library will use C++11.

endchoice

config UAVCAN_TINY
	bool "Tiny"
	default n
	---help---
		Removes some features to save memory.

config UAVCAN_TOSTRING
	bool "Implement toString"
	default n
	---help---
		The library will add a toString method to most of its classes.

config UAVCAN_IMPLEMENT_PLACEMENT_NEW
	bool "Implement Placement new"
	default n
	---help---
		The library will implement placement new.

config UAVCAN_USE_EXTERNAL_SNPRINTF
	bool "Use External snprintf"
	default n
	---help---
		The library will use an external snprintf.

config UAVCAN_USE_EXTERNAL_FLOAT16_CONVERSION
	bool "Use External float16 Conversion"
	default n
	---help---
		The library will use an external float16 conversion.

config UAVCAN_NO_ASSERTIONS
	bool "Disable Assertions"
	default n
	---help---
		Disables assertions.

config UAVCAN_MEM_POOL_BLOCK_SIZE
	int "Memory Pool Block Size"
	default 0
	---help---
		Specifies the memory pool block size.  If the value is 0, the
		library will use a default value.

config UAVCAN_RX_QUEUE_CAPACITY
	int "Rx Queue Capacity"
	default 0
	---help---
		Specifies the rx queue capacity.  If the value is 0, the
		library will use a default value.

config UAVCAN_BIT_RATE
	int "Bit Rate"
	default 0
	range 0 1000000
	---help---
		Specifies the CAN bit rate.  If the value is 0, the library
		will automatically detect the bit rate.

config UAVCAN_INIT_RETRIES
	int "Initialization Retries"
	default 0
	---help---
		Specifies the number of times to try initializing the CAN
		peripherals before panicking.  A value of 0 means to try
		forever.

endif
